#checking if items in the list is available on data or not
def must_have_one_of_the_following(data='Arpan Ghimire',must_have_one_of_the_values=[1,2,3,4,'Arpan']):

    #spliting items in data with white space and check if items on list is at data or not
    c=list(map(lambda x:  x in data.split(" "),must_have_one_of_the_values))

    # if any value is true it will return true
    if True in c:
        return 'True'
    else:
       return  'False'

# print(must_have_one_of_the_following())

