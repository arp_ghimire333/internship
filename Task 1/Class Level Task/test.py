from sample_1 import Sample

#instances
unique = Sample()
number = Sample()
mini = Sample()
max = Sample()
must = Sample()

#print
print(unique.is_unique('Arp'))
print(number.check_number(2))
print(mini.minimum_length("Arpan", 2))
print(max.maximum_length("Arpan", 2))
print(must.must_have_one_of_the_following())


