import check_number
import check_unique_number
import maximum_length
import minimum_length
import must_have_one_of_following

print(check_number.is_number())
print(minimum_length.minimum_length("Arpan Ghimire", 17))
print(must_have_one_of_following.must_have_one_of_the_following())
print(check_number.is_number())
print(maximum_length.maximum_length("Arpan",7))