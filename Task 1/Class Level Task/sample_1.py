import numbers


class Sample:
    # for checking unique number
    def is_unique(self, data, check_list=[1, 2, 3, 4, 5, 5, 'Arp', 'Arp']):
        # checking whether item is in list or not
        if data in check_list:
            c = check_list.count(data)
            # if c>1 then the number is not unique
            if c > 1:
                return False
            else:
                return True

        else:
            return False

    # for checking number
    def check_number(self, data):
        # checking whether the data is number or not
        return isinstance(data, numbers.Number)

    # for checking minimum length
    def minimum_length(self, data, length):
        # checking minimum length
        if len(data) >= length:
            return True
        else:
            return False
            # for checking minimum length

    def maximum_length(self, data, length):
        # checking maximum length
        if len(data) <= length:
            return True
        else:
            return False

    # checking if items in the list is available on data or not
    def must_have_one_of_the_following(self,data='Arpan Ghimire', must_have_one_of_the_values=[1, 2, 3, 4, 'Arpan']):

        # spliting items in data with white space and check if items on list is at data or not
        c = list(map(lambda x: x in data.split(" "), must_have_one_of_the_values))

        # if any value is true it will return true
        if True in c:
            return 'True'
        else:
            return 'False'

'''
# instances
unique = Sample()
number = Sample()
mini = Sample()
max = Sample()
must = Sample()

print(unique.is_unique('Arp'))
print(number.check_number(2))
print(mini.minimum_length("Arpan", 2))
print(max.maximum_length("Arpan", 2))
print(must.must_have_one_of_the_following())
'''